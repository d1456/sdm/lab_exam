const { request, response } = require('express');
const express = require('express')
const db  = require('../db')
const utils  = require('../utils')

const router  = express.Router();

router.get('/',(request,response)=>{

    const query = `select * from movie`;

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});


router.post('/',(request,response)=>{

    const {movie_title,movie_release_date,movie_time,director_name} = request.body;

    const query = `insert into movie(movie_title,movie_release_date,movie_time,director_name) values('${movie_title}','${movie_release_date}',
    '${movie_time}','${director_name}')`;

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});

router.patch('/update/:id',(request,response)=>{

    const {id} = request.params;
  
    const {movie_release_date,movie_time} = request.body;

    const query = `update movie set movie_release_date='${movie_release_date}',
         movie_time='${movie_time} where movie_id=${id}'`;

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});


router.delete('/delete/:id',(request,response)=>{

    const {id} = request.params;
  
    const query = `delete from movie where movie_id = ${id};`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});

module.exports = router;

